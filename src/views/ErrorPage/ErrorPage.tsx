

export default function ErrorPage() {
    return (
        <div className={"container text-center mt-20 text-2xl"}>
            <h1 className="font-bold text-3xl">Yahhh!</h1>
            <p>Xin lỗi, có lỗi xảy ra ở đây</p>
            <p>Bạn có thể tải lại trang hoặc kiểm tra đường dẫn</p>
        </div>
    );
}
