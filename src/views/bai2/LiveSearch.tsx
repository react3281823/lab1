import { FaSearch } from "react-icons/fa"
import {useState} from "react";
import {Button} from "@/components/ui/button.tsx";
import {useNavigate} from "react-router-dom";

function LiveSearch() {
    const [searchContent, setSearchContent] = useState('');
    const navigate = useNavigate();

    return (
        <div className={"container text-2xl text-center mt-10"}>
            <h1>Live Search: React Application</h1>
            <form onSubmit={(e) => e.preventDefault()} className="mt-10 w-max mx-auto">
                <label htmlFor="search" className="relative ">
                    <input id="search" placeholder={'Search'} className="w-[20rem] h-10 outline-none
                     border border-[rgba(0,0,0,0.5)] rounded px-5 py-5" type="text"
                           onChange={(e) => setSearchContent(e.target.value)} value={searchContent} />
                    <FaSearch className="absolute top-1 right-2 cursor-pointer" />
                </label>
            </form>
            <div className="text-xl mt-10">Search content : {searchContent}</div>
            <Button size={'lg'} onClick={() =>  navigate('/')} className={"mt-10"} >về trang chủ</Button>
        </div>
    );
}

export default LiveSearch;
