import {Button} from "@/components/ui/button.tsx";
import { useNavigate } from "react-router-dom";

function HelloWorld() {
    const navigate = useNavigate();

    return (
        <>
            <h1 className="text-3xl font-bold underline text-center mt-10">
                Hello world!
            </h1>
            <Button onClick={() => navigate('/')} size={'lg'} className="mx-auto w-max block mt-10 ">Về trang chủ !</Button>
        </>
    );
}

export default HelloWorld;
