import {useEffect, useState} from "react";
import {Button} from "@/components/ui/button.tsx";
import {useNavigate} from "react-router-dom";

function Time() {
    const navigate = useNavigate();
    const [time, setTime] = useState('');
    const getCurrentTime = () => {
        return new Date().toLocaleTimeString();
    }

    useEffect(() => {
        const interval = setInterval(() => {
            setTime(getCurrentTime());
        }, 1000);

        return () => clearInterval(interval);
    }, [time]);

    return (
        <div className={"container text-center text-2xl mt-20 mx-auto"}>
            <div className="font-bold">Current Time</div>
            <h3 className="mt-5 h-5">{time}</h3>
            <Button onClick={() => navigate('/')} className="mt-10">Trở về trang chủ</Button>
        </div>
    );
}

export default Time;
