import {Link} from "react-router-dom";


function Menu() {
    return (
        <div className="container">
            <h1 className="text-2xl mt-10 text-center font-semibold">Nguyễn Đinh Tiến Điền lab 1 - ReactJs</h1>
            <ul className={"w-max mx-auto text-2xl mt-5 flex gap-10"}>
                <li>
                    <Link to="/bai1">Bài 1</Link>
                </li>
                <li>
                    <Link to="bai2">Bài 2</Link>
                </li>
                <li>
                    <Link to="bai3">Bài 3</Link>
                </li>
            </ul>
        </div>
    );
}

export default Menu;
