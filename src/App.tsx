
import {Button} from "@/components/ui/button.tsx";


function App() {
    const onClick = () => {
        console.log('onClick button');
    }
    return (
        <>
            <h1 className="text-3xl font-bold underline text-center mt-10">
                Hello world!
            </h1>
            <Button size={'lg'} onClick={onClick} className="mx-auto w-max block mt-10 ">hello button !</Button>
        </>
    )
}

export default App
