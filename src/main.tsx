import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {createBrowserRouter,  RouterProvider} from 'react-router-dom';
import Menu from "@/views/home/menu.tsx";
import HelloWorld from "@/views/bai1/HelloWorld.tsx";
import ErrorPage from "@/views/ErrorPage/ErrorPage.tsx";
import LiveSearch from "@/views/bai2/LiveSearch.tsx";
import Time from "@/views/bai3/Time.tsx";


const router = createBrowserRouter([
    {
        path: "/",
        element: <Menu/>,
        errorElement: <ErrorPage/>
    },
    {
        path: "bai1",
        element: <HelloWorld/>,
    },
    {
        path: "bai2",
        element: <LiveSearch/>,
    },
    {
        path: "bai3",
        element: <Time/>,
    },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>,
)
